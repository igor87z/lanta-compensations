import logging
from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from .client_service import ClientService
from .logs.decorators import log
from .tariff import Tariff


@dataclass(frozen=True)
class Condition:
    condition_id: int
    tariff_id: Optional[int]
    tv_tariff_id: Optional[int]
    service_group_id: Optional[int]
    service_id: Optional[int]
    cost: Optional[float]
    activate: Optional[datetime]
    part: float
    force: Optional[bool] = False

    @log('Check condition for minimal set', level=logging.DEBUG)
    def __check_minimal_set(self) -> bool:
        return (
                       self.service_group_id is not None or
                       self.service_id is not None
               ) and (
                       self.tariff_id is not None or
                       self.tv_tariff_id is not None or
                       self.force
               )

    @log('Check service\'s group', level=logging.DEBUG)
    def __check_service_group(self, service: ClientService) -> bool:
        return self.service_group_id == service.service.group_id

    @log('Check service', level=logging.DEBUG)
    def __check_service(self, service: ClientService) -> bool:
        return self.service_id == service.service.service_id

    @log('Check individual compensation', level=logging.DEBUG)
    def __check_individual_compensation(self, service: ClientService) -> bool:
        return service.compensation

    @log('Check force condition', level=logging.DEBUG)
    def __is_force_condition(self):
        return self.force

    @log('Check tariff', level=logging.DEBUG)
    def __check_tariff(self, tariff: Tariff) -> bool:
        return (
                (
                        self.tariff_id is None and
                        tariff.tariff_id is None
                ) or
                self.tariff_id == tariff.tariff_id
        )

    @log('Check TV tariff', level=logging.DEBUG)
    def __check_tv_tariff(self, tariff: Tariff) -> bool:
        return (
                self.tv_tariff_id is None or
                (
                        tariff.tariff_id is None and
                        self.tv_tariff_id == tariff.tv_tariff_id
                )
        )

    @log('Check service cost', level=logging.DEBUG)
    def __check_service_cost(self, service: ClientService) -> bool:
        return (
                self.cost is None or
                self.cost == service.service.svc_cost or
                self.cost == service.cs_cost
        )

    @log('Check activate date set in condition', level=logging.DEBUG)
    def __activate_date_not_set(self) -> bool:
        return self.activate is None

    @log('Check service activate date', level=logging.DEBUG)
    def __check_service_activate_date(self, service: ClientService) -> bool:
        return (
                self.activate <= service.activate
        )

    @log('Check (tv) tariff activate date', level=logging.DEBUG)
    def __check_tariff_activate_date(self, tariff: Tariff) -> bool:
        return (
                (
                        tariff.tariff_id and
                        tariff.tariff_change and
                        self.activate <= tariff.tariff_change
                )
                or
                (
                        tariff.tv_tariff_id and
                        tariff.tv_tariff_change and
                        self.activate <= tariff.tv_tariff_change
                )
        )

    @log('Check activate date', level=logging.DEBUG)
    def __check_activate_date(self, service: ClientService,
                              tariff: Tariff) -> bool:
        return (
                self.__activate_date_not_set() or
                self.__check_service_activate_date(service) or
                self.__check_tariff_activate_date(tariff)
        )

    @log('Check service', level=logging.DEBUG)
    def check_service(self, service: ClientService, tariff: Tariff) -> bool:
        return (
                self.__check_minimal_set() and
                (
                        self.__check_service_group(service) or
                        self.__check_service(service)
                ) and
                (
                        (
                                self.__is_force_condition() and
                                self.__check_individual_compensation(service)
                        ) or
                        (
                                not self.__is_force_condition() and
                                self.__check_tariff(tariff) and
                                self.__check_tv_tariff(tariff) and
                                self.__check_service_cost(service) and
                                self.__check_activate_date(service, tariff)
                        )
                )

        )
