import logging
import os
from itertools import groupby
from typing import Any, Dict, List, Tuple

from psycopg2.extensions import cursor

from .bill import Bill
from .logs.decorators import log


class BillFactory:
    @log('Generate Bill from raw data', level=logging.DEBUG)
    def __bill_from_raw(self, bill: Dict[str, Any]) -> Bill:
        return Bill(
            client_id=bill['client_id'],
            bill_type_id=bill['bill_type_id'],
            ext_id=bill['ext_id'],
            ext_id_old=bill['ext_id_old'],
            ext_id_old2=bill['ext_id_old2'],
            comment=bill['comment'],
            amount=bill['amount']
        )

    @log('Get bills, raw data', level=logging.DEBUG)
    def __raw_data(self, db_cursor: cursor, sql_query: str,
                   client_ids: Tuple[int, ...]) -> List[Dict[str, Any]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'bills',
                               '{0}.sql'.format(sql_query)), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return db_cursor.fetchall()

    @log('Get clients\' bills', level=logging.DEBUG)
    def __bills(self, db_cursor: cursor, bill_type: str,
                client_ids: Tuple[int, ...]) -> Tuple[Bill, ...]:
        return tuple(
            map(
                self.__bill_from_raw,
                self.__raw_data(db_cursor, bill_type, client_ids)
            )
        )

    @log('Sort bills by clients', level=logging.DEBUG)
    def __sort_bills_by_client(self, bills):
        return sorted(
            bills,
            key=lambda bill: bill.client_id
        )

    def __clients_bills(self, db_cursor: cursor, bill_type: str,
                        client_ids: Tuple[int, ...]):
        return {
            client_id: tuple(bills)
            for client_id, bills in groupby(
                self.__sort_bills_by_client(
                    self.__bills(db_cursor, bill_type, client_ids)),
                lambda bill: bill.client_id
            )
        }

    @log('Get clients\' compensations(Bill objects)', level=logging.DEBUG)
    def compensations(self, db_cursor: cursor,
                      client_ids: Tuple[int, ...]) -> Dict[int, Tuple[Bill]]:
        """Get clients' compensations.

        Keyword arguments:
        db_cursor -- database connection's cursor
        client_ids -- clients' identifiers
        """
        return self.__clients_bills(db_cursor, 'compensations', client_ids)

    @log('Get clients\' compensations(Bill objects)', level=logging.DEBUG)
    def service_bills(self, db_cursor: cursor,
                      client_ids: Tuple[int, ...]) -> Dict[int, Tuple[Bill]]:
        """Get clients' service bills.

        Keyword arguments:
        db_cursor -- database connection's cursor
        client_ids -- clients' identifiers
        """
        return self.__clients_bills(db_cursor, 'service_bills', client_ids)
