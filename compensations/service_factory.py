import logging
import os
from datetime import datetime
from itertools import groupby
from typing import Any, Dict, List, Optional, Tuple

from psycopg2.extensions import cursor

from .client_service import ClientService
from .logs.decorators import log
from .service import Service


class ServiceFactory:
    @log('Format activate date', level=logging.DEBUG)
    def __format_activate_date(
            self, activate: Optional[datetime]) -> Optional[datetime]:
        if activate is not None and isinstance(activate, datetime):
            return datetime.combine(activate.date(), activate.time())
        return activate

    @log('Get services, raw data', level=logging.DEBUG)
    def __raw_services(self, db_cursor: cursor,
                       client_ids: Tuple[int]) -> List[Tuple[Dict[str, Any]]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'services', 'clients_services.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return db_cursor.fetchall()

    @log('Generate Service(object) from raw data', level=logging.DEBUG)
    def __service_from_raw(self, service: Dict[str, Any]) -> Service:
        return Service(
            service_id=service['service_id'],
            group_id=service['group_id'],
            svc_name=service['svc_name'],
            svc_cost=service['svc_cost'],
        )

    @log('Generate ClientService(object) from raw data', level=logging.DEBUG)
    def __client_service_from_raw(self, service) -> ClientService:
        return ClientService(
            client_id=service['client_id'],
            client_service_id=service['client_service_id'],
            service=self.__service_from_raw(service),
            cs_cost=service['cs_cost'],
            activate=self.__format_activate_date(service['activate']),
            compensation=service['compensation']
        )

    @log('Get clients\' services', level=logging.DEBUG)
    def __services(self, db_cursor: cursor,
                   client_ids: Tuple[int]) -> Tuple[ClientService, ...]:
        return tuple(
            map(
                self.__client_service_from_raw,
                self.__raw_services(db_cursor, client_ids)
            )
        )

    @log('Get services grouping by clients', level=logging.DEBUG)
    def services(self, db_cursor: cursor,
                 client_ids: Tuple[int, ...]) -> Dict[int, Tuple[ClientService, ...]]:
        return {
            client_id: tuple(services)
            for client_id, services in groupby(
                sorted(
                    self.__services(db_cursor, client_ids),
                    key=lambda service: service.client_id
                ),
                lambda service: service.client_id
            )
        }
