import logging
import os
from datetime import datetime, date, time
from itertools import repeat
from typing import Any, Dict, List, Optional, Tuple, Union

from psycopg2.extensions import cursor

from .compensation import Compensation
from .condition import Condition
from .logs.decorators import log


class CompensationFactory:
    @log('Get compensations, raw data', level=logging.DEBUG)
    def __raw_compensations(self,
                            db_cursor: cursor) -> List[Dict[str, Any]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'compensations', 'compensations.sql'), 'r') as query:
            db_cursor.execute(query.read())
            return db_cursor.fetchall()

    @log('Get compensation\'s conditions', level=logging.DEBUG)
    def __raw_conditions(self, db_cursor: cursor,
                         compensation_id: int) -> List[Dict[str, Any]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'compensations', 'conditions.sql'), 'r') as query:
            db_cursor.execute(query.read(), (compensation_id,))
            return db_cursor.fetchall()

    @log('Format value to float', level=logging.DEBUG)
    def __format_float(self, cost: Optional[
        Union[int, float, str]]) -> Optional[float]:
        if cost is not None:
            return float(cost)
        return cost

    @log('Format date', level=logging.DEBUG)
    def __format_activate_date(self, activate: Optional[
        Union[date, datetime]]) -> Optional[datetime]:
        if activate is not None and isinstance(activate, date):
            return datetime.combine(activate, time(0, 0, 0))
        return activate

    @log('Generate Condition(object) from raw data', level=logging.DEBUG)
    def __condition_from_raw(self, condition: Dict[str, Any]) -> Condition:
        return Condition(
            condition_id=condition['compensation_condition_id'],
            tariff_id=condition['tariff_id'],
            tv_tariff_id=condition['tv_tariff_id'],
            service_group_id=condition['service_group_id'],
            service_id=condition['service_id'],
            cost=self.__format_float(condition['svc_cost']),
            activate=self.__format_activate_date(
                condition['active_from']),
            part=self.__format_float(condition['part']),
            force=condition['force'],
        )

    @log('Generate Compensation(object) from raw data', level=logging.DEBUG)
    def __compensation_from_raw(self, db_cursor: cursor,
                                compensation: Dict[str, Any],
                                conditions: Optional[
                                    Tuple[int]]) -> Compensation:
        return Compensation(
            compensation_id=compensation['compensation_id'],
            name=compensation['name'],
            conditions=tuple(
                filter(
                    lambda condition: (not conditions or
                                       condition.condition_id in conditions),
                    map(
                        self.__condition_from_raw,
                        self.__raw_conditions(
                            db_cursor, compensation['compensation_id'])
                    )
                )
            )
        )

    @log('Get Compensations', level=logging.DEBUG)
    def compensations(self, db_cursor: cursor,
                      compensations: Optional[Tuple[int, ...]],
                      conditions: Optional[Tuple[int, ...]]) -> Tuple[Compensation, ...]:
        return tuple(
            filter(
                lambda compensation: (
                        (
                                not compensations or
                                compensation.compensation_id in compensations
                        ) and
                        compensation.conditions
                ),
                map(
                    self.__compensation_from_raw,
                    repeat(db_cursor),
                    self.__raw_compensations(db_cursor),
                    repeat(conditions)
                )
            )
        )
