select
    client_id,
    tarif_id as tariff_id,
    tv_tarif_id as tv_tariff_id,
    contract_date
from
    clients
    natural join account
where
    client_id in %s;
