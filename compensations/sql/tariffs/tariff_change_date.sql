select
    client_id,
    max(tarif_date) as change_date
from
    (
        select
            *,
            lead(tarif_id) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tarif_history_id desc) prev_tarif_id,
            lead(tarif_date) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tarif_history_id desc) prev_tarif_date,
            lead(client_id) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tarif_history_id desc) prev_client_id
        from
            tarif_history
            join clients using(client_id)
        where
            client_id in %s
        order by
            client_id,
            tarif_date desc,
            tarif_history_id desc
    ) t
where
    (
        client_id <> prev_client_id or
        tarif_id is null or
        prev_tarif_id is null or
        tarif_id <> prev_tarif_id
    ) and
    not(
        tarif_id is null and
        prev_tarif_id is null
    )
group by
    client_id;
