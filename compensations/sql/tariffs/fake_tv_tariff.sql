select
    distinct client_id
from
    account
    join net.pon using(client_id)
where
    tarif_id is null and
    tv_tarif_id is not null;
