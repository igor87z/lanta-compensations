select
    distinct client_id
from
    services
    join clients_services using(service_id)
    join account using(client_id)
where
    state = 1 and
    (group_id = 11 or service_type = 'cctv') and
    tarif_id is null and
    tv_tarif_id is null;
