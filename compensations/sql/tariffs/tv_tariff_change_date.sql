select
    client_id,
    max(tarif_date) as change_date
from
    (
        select
            *,
            lead(tv_tarif_id) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tv_tarif_history_id desc) prev_tv_tarif_id,
            lead(tarif_date) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tv_tarif_history_id desc) prev_tarif_date,
            lead(client_id) over(order by
                                    client_id,
                                    tarif_date desc,
                                    tv_tarif_history_id desc) prev_client_id
        from
            tv.tv_tarif_history
            join clients using(client_id)
        where
            client_id in %s
        order by
            tarif_date desc,
            tv_tarif_history_id desc
    ) t
where
    (
        client_id <> prev_client_id or
        t.tv_tarif_id is null or
        prev_tv_tarif_id is null or
        t.tv_tarif_id <> prev_tv_tarif_id
    ) and
    not(
        t.tv_tarif_id is null and
        prev_tv_tarif_id is null
    )
group by
    client_id;
