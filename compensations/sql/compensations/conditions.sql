select
    compensation_condition_id,
    tarif_id as tariff_id,
    tv_tarif_id as tv_tariff_id,
    service_group_id,
    service_id,
    part,
    case
        when compensation_id = 1 and
                (tarif_id is null or
                 tarif_id not in (365, 371, 377, 378, 379, 380)) then
            coalesce(
                active_from,
                date'2019-01-15'
            )
        else
            active_from
    end as active_from,
    svc_cost,
    "force"
from
    promotions.compensation_conditions
where
    compensation_condition_id not in (8, 9, 10, 13, 14, 19, 20,
                                      85, 86, 87, 90, 91, 96, 97, 124) and
    compensation_id = %s;
