select
    client_id,
    balance,
    bonus,
    case
        when credit_date >= now() then
            credit
        else 0
    end as credit,
    blocked,
    coalesce(
        (blocked and auto_block <> ''),
        false
        ) as auto_blocked
from
    clients cl
    natural join account
where
    break_date is null and
    mod(client_id, %s) = %s and
    client_id >= %s and
    client_id in (
        select
            client_id
        from
            clients_services
            join services using(service_id)
        where
            state = 1 and
            (
                compensation or
                service_id in (
                    select
                        distinct service_id
                    from
                        promotions.compensation_conditions
                    where
                        service_id is not null
                ) or
                group_id in (
                    select
                        distinct service_group_id
                    from
                        promotions.compensation_conditions
                    where
                        service_group_id is not null
                )
            )
    )
order by
    client_id
limit %s;
