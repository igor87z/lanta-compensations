select
    distinct client_id
from
    once_bills
where
    bill_type_id in (1, 7) and
    client_id in %s;
