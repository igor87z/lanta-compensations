select
    client_id,
    sum(amount) as debt
from
    clients_services cs
    join invoices using(client_service_id)
where
    bill_id is null and
    client_id in %s
group by
    client_id;
