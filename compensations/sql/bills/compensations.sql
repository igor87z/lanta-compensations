select
    client_id,
    bill_type_id,
    ext_id,
    ext_id_old,
    ext_id_old2,
    comment,
    amount
from
    bills
where
    date_trunc('month', bill_date) = date_trunc('month', now()) and
    bill_type_id in (1, 7) and
    ext_id is not null and
    amount < 0 and
    client_id in %s;
