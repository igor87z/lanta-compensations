select
    client_id,
    client_service_id,
    service_id,
    group_id,
    svc_name,
    svc_cost,
    cs_cost,
    compensation,
    coalesce((
        select
            min(action_time)
        from
            service_history sh
        where
            sh.client_service_id = cs.client_service_id and
            action = 1
    ), now()) as activate
from
    clients_services cs
    join services using(service_id)
where
    (
        service_id in (
            select
                distinct service_id
            from
                promotions.compensation_conditions
            where
                service_id is not null
        ) or
        group_id in (
            select
                distinct service_group_id
            from
                promotions.compensation_conditions
            where
                service_group_id is not null
        )
    ) and
    state = 1 and
    client_id in %s;
