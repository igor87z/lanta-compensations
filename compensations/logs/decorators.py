import logging
import time
import traceback
from functools import wraps

from .logger import logger


def log(operation: str, level: int = logging.DEBUG):
    """Logging decorator.

    Keyword arguments:
    operation -- describe original function's operation
    level -- log level (default DEBUG, 10)
    """

    def _log(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            logger().log(level, 'Call function %s: %s',
                         func.__qualname__, operation)
            if not args and not kwargs:
                logger().log(level, 'No arguments')
            else:
                if args:
                    logger().log(level, 'Args:')
                    for arg in args:
                        logger().log(level, '\t%s\t%s', type(arg), arg)
                if kwargs:
                    logger().log(level, 'Kwargs:')
                    for key, value in args:
                        logger().log(level, '\t%s: %s\t"%s"',
                                     key, type(value), value)
            try:
                start_time = time.time()
                result = func(*args, **kwargs)
            except Exception as exp:
                logger().exception(
                    'Exception in call "%s": %s', func.__name__, exp)
                logger().exception(traceback.format_exc())
                raise exp
            else:
                logger().log(level, 'Result function "%s": %s',
                             func.__name__, result)
                logger().debug('Working time: %s second(s)', time.time() - start_time)
            return result

        return wrapper

    return _log
