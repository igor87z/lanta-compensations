import logging
import sys


def logger(level: int = logging.ERROR, filename: str = ''):
    """Get logger. Create new one at first call,
    all other calls returns the same object.

    Keyword arguments:
    level -- log level (default ERROR, 40)
    filename -- log file, if not set(None or empty string)
        print to stdout/stderr (default '')
    """
    if not hasattr(logger, '__logger'):
        __logger = logging.getLogger('Compensations')
        __logger.setLevel(level)

        formatter = logging.Formatter(
            '%(asctime)s - [PID:%(process)d|TID:%(thread)d] - '
            '[%(levelname)s] - %(message)s')

        if not filename:
            stdout_handler = logging.StreamHandler(sys.stdout)
            stdout_handler.setLevel(logging.DEBUG)
            stdout_handler.setFormatter(formatter)

            stderr_handler = logging.StreamHandler(sys.stderr)
            stderr_handler.setLevel(logging.ERROR)
            stderr_handler.setFormatter(formatter)

            __logger.addHandler(stdout_handler)
            __logger.addHandler(stderr_handler)
        else:
            file_handler = logging.FileHandler(filename)
            file_handler.setLevel(logging.DEBUG)
            file_handler.setFormatter(formatter)

            __logger.addHandler(file_handler)

        logger.__logger = __logger

    return logger.__logger
