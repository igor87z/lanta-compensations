from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass(frozen=True)
class Tariff:
    tariff_id: Optional[int]
    tariff_change: Optional[datetime]
    tv_tariff_id: Optional[int]
    tv_tariff_change: Optional[datetime]
