import logging
from dataclasses import dataclass
from itertools import groupby, repeat
from typing import Tuple

from .bill import Bill, NONE_BILL
from .client_service import ClientService
from .compensation import Compensation
from .logs.decorators import log
from .tariff import Tariff


@dataclass(frozen=True)
class Client:
    client_id: int
    balance: float
    credit: float
    debt: float
    bonus: float
    blocked: bool
    auto_blocked: bool
    new_contract: bool
    tariff: Tariff
    services: Tuple[ClientService, ...]
    exists_compensations: Tuple[Bill, ...]
    service_bills: Tuple[Bill, ...]

    @log('Check bill already invoiced', level=logging.DEBUG)
    def __check_bill_exists(self, bill: Bill) -> bool:
        return any(
            map(
                lambda b: b.same(bill),
                self.exists_compensations
            )
        )

    def __check_compensationable_bill(self, bill: Bill) -> bool:
        return any(
            map(
                lambda b: b.can_compensate(bill),
                self.service_bills
            )
        )

    @log('Check services for compensate', level=logging.DEBUG)
    def __check_services(self, compensation: Compensation) -> Tuple[Bill, ...]:
        return tuple(
            filter(
                lambda b: b != NONE_BILL,
                map(
                    compensation.bill,
                    self.services,
                    repeat(self.tariff),
                    repeat(self.client_id),
                )
            )
        )

    @log('Generate compensation Bills(objects)', level=logging.DEBUG)
    def __make_compensation_bills(
            self, compensations: Tuple[Compensation]) -> Tuple[Bill, ...]:
        return tuple(
            bill
            for bills in map(
                self.__check_services,
                compensations
            )
            for bill in bills
        )

    @log('Sort bills by compensations and services', level=logging.DEBUG)
    def __sort_bills(self, bills: Tuple[Bill, ...]) -> Tuple[Bill, ...]:
        return tuple(sorted(bills,
                            key=lambda bill: (bill.ext_id_old,
                                              bill.ext_id_old2)))

    @log('Filter Bills for unique', level=logging.DEBUG)
    def __unique_bills(self, bills: Tuple[Bill]) -> Tuple[Bill, ...]:
        force = tuple(filter(lambda bill: bill.force, bills))
        compensations = set(map(lambda bill: bill.ext_id_old, force))
        return tuple(
            tuple(bls)[0] for _, bls in groupby(
                self.__sort_bills(
                    tuple(filter(
                        lambda bill: (
                                bill not in force and
                                bill.ext_id_old not in compensations),
                        bills
                    ))
                ),
                lambda bill: bill.ext_id_old
            )
        ) + force

    @log('Get needed compensations', level=logging.DEBUG)
    def __needed_bills(
            self, compensations: Tuple[Compensation]) -> Tuple[Bill, ...]:
        return tuple(
            filter(
                lambda bill: (not self.__check_bill_exists(bill) and
                              self.__check_compensationable_bill(bill)),
                self.__unique_bills(
                    self.__make_compensation_bills(compensations)
                )
            )
        )

    @log('Check client state', level=logging.DEBUG)
    def __check_client(self, bills: Tuple[Bill]) -> Tuple[Bill, ...]:
        if (
                (
                        self.auto_blocked and
                        (
                                self.balance +
                                self.credit +
                                self.bonus -
                                self.debt - sum(
                            map(
                                lambda bill: bill.amount,
                                bills
                            )
                        ) > -1
                        )
                ) or
                not self.blocked or
                self.new_contract
        ):
            return bills
        return ()

    @log('Get client\'s compensation bills', level=logging.DEBUG)
    def compensations(self, compensations: Tuple[Compensation]) -> Tuple[Bill, ...]:
        """Get compensation bills.

        Keyword arguments:
        compensations -- exists compensations
        """
        return self.__check_client(
            self.__needed_bills(compensations)
        )
