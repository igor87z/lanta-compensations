import logging
import os
from itertools import repeat
from typing import Any, Dict, Optional, Tuple, Iterator

from psycopg2.extensions import cursor

from .bill import Bill
from .bill_factory import BillFactory
from .client import Client
from .client_service import ClientService
from .logs.decorators import log
from .service_factory import ServiceFactory
from .tariff import Tariff
from .tariff_factory import TariffFactory


class ClientFactory:
    @log('Get invoices\' debts', level=logging.DEBUG)
    def __invoice_debt(self, db_cursor: cursor,
                       client_ids: Tuple[int, ...]) -> Dict[int, float]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'clients', 'invoice_debt.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return {
                client['client_id']: client['debt']
                for client in db_cursor.fetchall()
            }

    @log('Get clients\' raw data', level=logging.DEBUG)
    def __raw_clients(self, db_cursor: cursor, parts: int, part_number: int,
                      start: int, part_size: int,
                      client_ids: Optional[Tuple[int]]) -> Tuple[Dict[str, Any]]:
        if client_ids:
            filename = 'clients_by_id.sql'
            options = (parts, part_number, start, client_ids, part_size)
        else:
            filename = 'clients.sql'
            options = (parts, part_number, start, part_size)

        with open(os.path.join(os.path.dirname(__file__), 'sql', 'clients', filename), 'r') as query:
            db_cursor.execute(query.read(), options)
            return db_cursor.fetchall()

    @log('Get activated(not new) contracts', level=logging.DEBUG)
    def __activated_clients(self, db_cursor: cursor,
                            client_ids: Tuple[int, ...]) -> Tuple[int, ...]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'clients', 'activated.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return tuple(
                client['client_id'] for client in db_cursor.fetchall()
            )

    @log('Generate Client object from raw data', level=logging.DEBUG)
    def __client_from_raw(self, client: Dict[str, Any],
                          activated: Tuple[int],
                          debts: Dict[int, float],
                          tariffs: Dict[int, Tariff],
                          services: Dict[int, Tuple[ClientService]],
                          compensations: Dict[int, Tuple[Bill]],
                          service_bills: Dict[int, Tuple[Bill]]) -> Client:
        return Client(
            client_id=client['client_id'],
            balance=client['balance'],
            credit=client['credit'],
            debt=debts.get(client['client_id'], 0),
            bonus=float(client['bonus']),
            blocked=client['blocked'],
            auto_blocked=client['auto_blocked'],
            new_contract=client['client_id'] not in activated,
            tariff=tariffs.get(client['client_id']),
            services=services.get(client['client_id'], ()),
            exists_compensations=compensations.get(client['client_id'], ()),
            service_bills=service_bills.get(client['client_id'], ())
        )

    @log('Get clients', level=logging.DEBUG)
    def clients(self, db_cursor: cursor, parts: int,
                part_number: int, part_size: int,
                client_ids: Optional[Tuple[int]]) -> Iterator[Tuple[Client, ...]]:
        """Get clients by groups.

        Keyword arguments:
        db_cursor -- database connection's cursor
        parts -- parts count
        part_number -- part's number
        part_size -- clients count in part
        client_ids -- clients' identifiers
        """
        start = 0
        while True:
            clients = self.__raw_clients(db_cursor, parts, part_number,
                                         start, part_size, client_ids)
            found_client_ids = tuple(client['client_id'] for client in clients)
            if not found_client_ids:
                break
            start = max(found_client_ids) + 1
            activated = self.__activated_clients(db_cursor, found_client_ids)
            debt = self.__invoice_debt(db_cursor, found_client_ids)
            tariffs = TariffFactory().tariffs(db_cursor, found_client_ids)
            services = ServiceFactory().services(db_cursor, found_client_ids)
            compensations = BillFactory().compensations(db_cursor, found_client_ids)
            service_bills = BillFactory().service_bills(db_cursor, found_client_ids)
            yield tuple(
                map(
                    self.__client_from_raw,
                    clients,
                    repeat(activated),
                    repeat(debt),
                    repeat(tariffs),
                    repeat(services),
                    repeat(compensations),
                    repeat(service_bills),
                )
            )
            if len(clients) < part_size:
                break
