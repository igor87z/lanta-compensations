from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class Service:
    service_id: int
    group_id: Optional[int]
    svc_name: str
    svc_cost: float
