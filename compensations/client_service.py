from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from .service import Service


@dataclass(frozen=True)
class ClientService:
    client_id: int
    client_service_id: int
    service: Service
    cs_cost: Optional[float]
    activate: datetime
    compensation: bool
