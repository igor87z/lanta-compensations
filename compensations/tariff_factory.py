import logging
import os
from datetime import datetime, date, time
from typing import Any, Dict, List, Optional, Tuple

from psycopg2.extensions import cursor

from .logs.decorators import log
from .tariff import Tariff


class TariffFactory:
    @log('Get accounts\' base info, raw data', level=logging.DEBUG)
    def __base_info_raw(self, db_cursor: cursor,
                        client_ids: Tuple[int]):
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'tariffs', 'base_info.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return db_cursor.fetchall()

    @log('Get account\'s base info grouping by clients', level=logging.DEBUG)
    def __base_info(self, db_cursor: cursor,
                    client_ids: Tuple[int]):
        return {
            client['client_id']: client
            for client in self.__base_info_raw(db_cursor, client_ids)
        }

    @log('Get clients with fake cctv tariff, raw date', level=logging.DEBUG)
    def __clients_with_fake_cctv_tariff_raw(self, db_cursor: cursor) -> List[
        Tuple[Dict[str, Any]]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'tariffs', 'fake_cctv_tariff.sql'), 'r') as query:
            db_cursor.execute(query.read())
            return db_cursor.fetchall()

    @log('Get clients with fake cctv tariff', level=logging.DEBUG)
    def __clients_with_fake_cctv_tariff(self, db_cursor: cursor) -> Tuple[int, ...]:
        return tuple(
            map(
                lambda client: client['client_id'],
                self.__clients_with_fake_cctv_tariff_raw(db_cursor)
            )
        )

    @log('Get clients with fake TV tariff, raw data', level=logging.DEBUG)
    def __clients_with_fake_tv_tariff_raw(self, db_cursor: cursor) -> List[
        Tuple[Dict[str, Any]]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'tariffs', 'fake_tv_tariff.sql'), 'r') as query:
            db_cursor.execute(query.read())
            return db_cursor.fetchall()

    @log('Get clients with fake TV tariff', level=logging.DEBUG)
    def __clients_with_fake_tv_tariff(self, db_cursor: cursor) -> Tuple[int, ...]:
        return tuple(
            map(
                lambda client: client['client_id'],
                self.__clients_with_fake_tv_tariff_raw(db_cursor)
            )
        )

    @log('Get last tariff change date', level=logging.DEBUG)
    def __tariffs_change_date(
            self, db_cursor: cursor,
            client_ids: Tuple[int]) -> Dict[int, Dict[str, Any]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'tariffs', 'tariff_change_date.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return {
                client['client_id']: client['change_date']
                for client in db_cursor.fetchall()
            }

    @log('Get last TV tariff change date', level=logging.DEBUG)
    def __tv_tariffs_change_date(
            self, db_cursor: cursor,
            client_ids: Tuple[int]) -> Dict[int, Dict[str, Any]]:
        with open(os.path.join(os.path.dirname(__file__), 'sql', 'tariffs', 'tv_tariff_change_date.sql'), 'r') as query:
            db_cursor.execute(query.read(), (client_ids,))
            return {
                client['client_id']: client['change_date']
                for client in db_cursor.fetchall()
            }

    @log('Get tariff identifier', level=logging.DEBUG)
    def __tariff_id(self, client: Dict[str, Any],
                    fake_cctv_tariff: Tuple[int],
                    fake_tv_tariff: Tuple[int]) -> Optional[int]:
        if client['tariff_id'] is None:
            if client['client_id'] in fake_cctv_tariff:
                return -1
            if client['client_id'] in fake_tv_tariff:
                return -2
        return client['tariff_id']

    @log('Get TV tariff identifier', level=logging.DEBUG)
    def __tv_tariff_id(self, tariff: Dict[str, Any]) -> Optional[int]:
        return tariff['tv_tariff_id']

    @log('Format activate date', level=logging.DEBUG)
    def __format_activate_date(
            self, activate: Optional[datetime]) -> Optional[datetime]:
        if activate is not None and isinstance(activate, date):
            return datetime.combine(activate, time(0, 0, 0))
        return activate

    @log('Get tariff change date', level=logging.DEBUG)
    def __tariff_change_date(self, tariff_id: Optional[int],
                             contract_date: datetime,
                             tariff_activate: Optional[
                                 datetime]) -> Optional[datetime]:
        if tariff_id:
            return self.__format_activate_date(
                tariff_activate or contract_date)
        return None

    @log('Get TV tariff change date', level=logging.DEBUG)
    def __tv_tariff_change_date(self,
                                tv_tariff_id: Optional[int],
                                contract_date: datetime,
                                tv_tariff_activate: Optional[
                                    datetime]) -> Optional[datetime]:
        if tv_tariff_id is not None:
            return self.__format_activate_date(
                tv_tariff_activate or contract_date)
        return None

    @log('Generate Tariff(object) from raw data', level=logging.DEBUG)
    def __tariff_from_raw(self,
                          client: Dict[str, Any],
                          tariff_activate: Optional[datetime],
                          tv_tariff_activate: Optional[datetime],
                          fake_cctv_tariff: Tuple[int],
                          fake_tv_tariff: Tuple[int]) -> Tariff:
        return Tariff(
            tariff_id=self.__tariff_id(client, fake_cctv_tariff, fake_tv_tariff),
            tariff_change=self.__tariff_change_date(
                self.__tariff_id(client, fake_cctv_tariff, fake_tv_tariff),
                client['contract_date'],
                tariff_activate),
            tv_tariff_id=self.__tv_tariff_id(client),
            tv_tariff_change=self.__tv_tariff_change_date(
                self.__tv_tariff_id(client),
                client['contract_date'],
                tv_tariff_activate)
        )

    @log('Get clients\' tariffs', level=logging.DEBUG)
    def tariffs(self, db_cursor: cursor,
                client_ids: Tuple[int, ...]) -> Dict[int, Tariff]:
        base_info = self.__base_info(db_cursor, client_ids)
        tariffs_raw = self.__tariffs_change_date(db_cursor, client_ids)
        tv_tariffs_raw = self.__tv_tariffs_change_date(db_cursor, client_ids)
        fake_cctv_tariff = self.__clients_with_fake_cctv_tariff(db_cursor)
        fake_tv_tariff = self.__clients_with_fake_tv_tariff(db_cursor)
        return {
            client_id: self.__tariff_from_raw(
                base_info.get(client_id),
                tariffs_raw.get(client_id),
                tv_tariffs_raw.get(client_id),
                fake_cctv_tariff,
                fake_tv_tariff
            )
            for client_id in client_ids
        }
