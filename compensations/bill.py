import logging
from dataclasses import dataclass
from typing import Optional

from .logs.decorators import log


@dataclass(frozen=True)
class Bill:
    client_id: int
    bill_type_id: int
    ext_id: Optional[int]
    ext_id_old: Optional[int]
    ext_id_old2: Optional[int]
    comment: str
    amount: float
    force: bool = False

    @log('Compare clients\' identifiers', level=logging.DEBUG)
    def __check_client(self, bill: 'Bill') -> bool:
        return self.client_id == bill.client_id

    @log('Compare bills\' types', level=logging.DEBUG)
    def __check_bill_type(self, bill: 'Bill') -> bool:
        return (
                self.bill_type_id in (1, 7) and
                bill.bill_type_id in (1, 7)
        )

    @log('Compare bills\' services', level=logging.DEBUG)
    def __check_service(self, bill: 'Bill') -> bool:
        return (
                self.ext_id == bill.ext_id and
                (
                        self.ext_id_old == bill.ext_id_old
                        or
                        self.ext_id_old is None
                        or
                        bill.ext_id_old is None
                ) and
                (
                        self.ext_id_old2 == bill.ext_id_old2
                        or
                        self.ext_id_old2 is None
                        or
                        bill.ext_id_old2 is None
                )
        )

    @log('Compare bills\' amounts', level=logging.DEBUG)
    def __check_amount(self, bill: 'Bill') -> bool:
        return self.amount < 0 and bill.amount < 0

    @log('Compare bills', level=logging.DEBUG)
    def same(self, bill: 'Bill') -> bool:
        """Compare bills by client identifier, bill's type, service and amount.

        Keyword arguments:
        bill -- other one bill
        """
        return (
                self.__check_client(bill) and
                self.__check_bill_type(bill) and
                self.__check_service(bill) and
                self.__check_amount(bill)
        )

    @log('Check current and compensate bill\'s type', level=logging.DEBUG)
    def __can_compensate_with_bill_type(self, bill: 'Bill'):
        return (
                self.bill_type_id == 4 and
                bill.bill_type_id in (1, 7) and
                self.ext_id == bill.ext_id
        )

    @log('Check compensate bill\'s amount', level=logging.DEBUG)
    def __can_compensate_with_amount(self, bill: 'Bill'):
        return (
                self.amount >= 0 and
                round(abs(self.amount + bill.amount), 2) <= 0.01
        )

    @log('Check can compensate', level=logging.DEBUG)
    def can_compensate(self, bill: 'Bill'):
        """Can compensate current bill with new(from argument).

        Keyword arguments:
        bill -- other one bill
        """
        return (
                self.__check_client(bill) and
                self.__can_compensate_with_bill_type(bill) and
                self.__can_compensate_with_amount(bill)
        )


NONE_BILL = Bill(
    client_id=0,
    bill_type_id=0,
    ext_id=0,
    ext_id_old=0,
    ext_id_old2=0,
    comment='',
    amount=0
)
