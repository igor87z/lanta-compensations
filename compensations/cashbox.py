import logging
from datetime import date
from itertools import groupby
from typing import Any, Tuple

from psycopg2.extensions import cursor

from .bill import Bill
from .logs.decorators import log
from .logs.logger import logger


class CashBox:
    @log('Prepare insert query, without arguments', logging.DEBUG)
    def __prepare_insert(self, bills_count: int) -> str:
        return (
                   'insert into bills('
                   'bill_type_id, client_id, bill_date, '
                   'comment, amount, ext_id, '
                   'ext_id_old, ext_id_old2)\nvalues\n'
               ) + (
                   ',\n'.join(
                       (
                           '({})'.format(
                               ', '.join(('%s',) * 8)
                           ),
                       ) * bills_count
                   )
               ) + ';'

    @log('Prepare update query, without arguments', logging.DEBUG)
    def __prepare_update(self) -> str:
        return (
            'update account '
            'set balance = balance + %s '
            'where client_id = %s;'
        )

    @log('Prepare sql query', logging.DEBUG)
    def __prepare_query(self, bills_count: int) -> str:
        return '\n'.join(
            (
                self.__prepare_insert(bills_count),
                self.__prepare_update()
            )
        )

    @log('Count compensations sum', logging.DEBUG)
    def __total(self, bills: Tuple[Bill, ...]) -> float:
        return round(
            sum(
                tuple(
                    map(
                        lambda b: b.amount,
                        bills
                    )
                )
            ),
            2
        )

    @log('Prepare arguments for sql query', logging.DEBUG)
    def __prepare_arguments(self, client_id: int,
                            bills: Tuple[Bill]) -> Tuple[Any, ...]:
        return tuple(
            opt
            for bill in bills
            for opt in (
                bill.bill_type_id,
                client_id,
                date.today(),
                bill.comment,
                bill.amount,
                bill.ext_id,
                bill.ext_id_old,
                bill.ext_id_old2,
            )
        ) + (
                   -self.__total(bills),
                   client_id,
               )

    @log('Invoice client\'s bills', logging.DEBUG)
    def __client_bills_invoice(self, db_cursor: cursor,
                               client_id: int,
                               bills: Tuple[Bill, ...],
                               disable_invoice: bool) -> None:
        query = self.__prepare_query(len(bills))
        args = self.__prepare_arguments(client_id, bills)
        logger().debug('Invoice bill sql query:')
        logger().debug(db_cursor.mogrify(query, args).decode('utf8'))
        if not disable_invoice:
            db_cursor.execute(query, args)
            db_cursor.connection.commit()

    @log('Invoice clients\' bills', logging.DEBUG)
    def invoice(self, db_cursor: cursor, bills: Tuple[Bill, ...],
                disable_invoice: bool) -> None:
        """Invoice bills.

        Keyword arguments:
        db_cursor -- database connection's cursor
        bills -- invoicing bills
        disable_invoice -- don't invoice, just prepare queries
        """
        logger().info('Invoice bills:')
        for bill in bills:
            logger().info('\t%s', bill)
        if disable_invoice:
            logger().info('Invoice disabled')
        for client_id, client_bills in groupby(
                sorted(bills, key=lambda b: b.client_id),
                lambda b: b.client_id
        ):
            self.__client_bills_invoice(
                db_cursor, client_id, tuple(client_bills), disable_invoice
            )
