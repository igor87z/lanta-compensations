import logging
import math
from calendar import monthrange
from dataclasses import dataclass
from datetime import date
from typing import Tuple, Optional

from .bill import Bill, NONE_BILL
from .client_service import ClientService
from .condition import Condition
from .logs.decorators import log
from .tariff import Tariff


@dataclass(frozen=True)
class Compensation:
    compensation_id: int
    name: str
    conditions: Tuple[Condition, ...]

    @log('Check service on compensation\'s conditions', level=logging.DEBUG)
    def __check_condition(self, service: ClientService,
                          tariff: Tariff) -> Optional[Condition]:
        for condition in self.conditions:
            if condition.check_service(service, tariff):
                return condition

    @log('Get compensate part', level=logging.DEBUG)
    def __part(self, service: ClientService) -> float:
        month = date.today().replace(day=1)
        if service.activate.date().replace(day=1) == month:
            days = monthrange(month.year, month.month)[1]
            return (days - service.activate.day + 1) / days
        return 1

    @log('Get service amount', level=logging.DEBUG)
    def __service_cost(self, service: ClientService) -> float:
        if service.cs_cost:
            return service.cs_cost
        return service.service.svc_cost

    @log('Get service amount', level=logging.DEBUG)
    def __amount(self, service: ClientService) -> float:
        return self.__service_cost(service) * self.__part(service)

    @log('Get bill\'s type', level=logging.DEBUG)
    def __bill_type(self, tariff: Tariff) -> int:
        if tariff.tv_tariff_id is not None and tariff.tariff_id is None:
            return 7
        return 1

    @log('Get compensation\'s Bill(object)', level=logging.DEBUG)
    def bill(self, service: ClientService, tariff: Tariff,
             client_id: int) -> Optional[Bill]:
        """Get compensation bill.
        Return NONE_BILL if does not fit under one of the conditions.

        Keyword arguments:
        service -- client's service
        tariff -- client's tariff
        client_id -- client identifier
        """
        condition = self.__check_condition(service, tariff)
        if condition:
            return Bill(
                client_id=client_id,
                bill_type_id=self.__bill_type(tariff),
                ext_id=service.service.service_id,
                ext_id_old=self.compensation_id,
                ext_id_old2=service.client_service_id,
                comment=self.name,
                force=service.compensation,
                # Round amount like bill's amount at activate service
                amount=-math.floor(self.__amount(service) * condition.part * 100.) / 100.
            )
        return NONE_BILL
