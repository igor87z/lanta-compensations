# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Multiprocesses compensation bills maker for equipment rent
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  * prepare environment:
  ```bash
  python -m venv venv
  source venv/bin/activate
  pip install -r requirements.txt
   ```
  * run
  ```bash
  python compensate.py --db-host DB_HOST --db-name DB_NAME --db-user DB_USER --db-pass DB_PASS [--db-port DB_PORT] [--procs PROCS] [--parts [PARTS [PARTS ...]]]
  ```
  * example
  ```bash
  python compensate.py --db-host 127.0.0.1 --db-name postgres --db-user user --db-pass password --procs 2 --parts 0 1
  ```
  * example for one client with full logs printing to stdout/stderr
  ```bash
  python compensate.py --db-host 127.0.0.1 --db-name postgres --db-user user --db-pass password --log-level 10 --clients 1234
  ```
* Configuration
  * --db-host - database host address, name or ip, required;
  * --db-name - database name, required;
  * --db-user - username for login, required;
  * --db-pass - password for login, required;
  * --db-port - database port for connect, default 5432, optional;
  * --procs - processes number, default: 1;
  * --parts - processing parts separated by space(started from 0). Negative values and values equal procs or greater ignoring;
  * --part-size - clients get from database by parts by set size. Default: 1000;
  * --compensations - processing compensations identifiers separated by space, optional. By default, processing all compensations;
  * --conditions - processing conditions identifiers separated by space, optional. By default, processing all compensations;
  * --clients - clients' identifiers. Processing only set clients. If set, for --procs, --parts using default values, --part-size equal set clients;
  * --disable-invoice - if set disable invoice bills, just count. Default: False, invoicing bills;
  * --log-level - log level: 0 - NOTSET, 10 - DEBUG, 20 - INFO, 30 - WARNING, 40 - ERROR, 50 - CRITICAL. Default: 20;
  * --log-file - log file's path. By default log print to stdout/stderr.
* Dependencies
  * PostgreSQL 9+;
  * Python 3.7+;
  * psycopg2-binary;
  * flake8 (for develop);
  * pytest (for develop).
* Database configuration
  * table account, columns:
    * client_id - client identifier;
    * balance - current balance;
    * credit - credit value;
    * credit_date - date till credit active;
    * bonus - current activated bonus;
    * blocked - blocked(True) or unblocked(False) right now;
    * tarif_id - current internet tariff identifier;
    * tv_tarif_id - current TV tariff identifier;
  * table bills, columns:
    * client_id - client identifier;
    * bill_date - bill's date;
    * bill_type_id - bill type identifier;
    * ext_id - service identifier;
    * ext_id_old - compensation identifier;
    * ext_id_old2 - client's service identifier;
    * comment - bill's comment;
    * amount - bill's amount;
  * table clients, columns
    * client_id - client identifier;
    * contract_date - create contract(account) date;
    * break_date - contract break date, if it was;
  * table clients_services, columns:
    * client_id - client identifier;
    * client_service_id - client's service identifier;
    * service_id - service (type) identifier;
    * cs_cost - force service cost;
    * compensation - force service compensation;
  * table services, columns:
    * service_id - service (type) identifier;
    * group_id - service group identifier;
    * svc_name - service name;
    * svc_cost - service (base) cost;
  * table service_history, columns:
    * client_service_id - client's service identifier;
    * action - action code:
      * 0 - create;
      * 1 - activate;
      * 2 - suspend;
      * 3 or 4 - disable;
    * reason - comment for action;
    * who - login who did;
    * action_time - action time;
  * table tarifs, columns:
    * tarif_id - internet tariff identifier;
    * need_manual_check - need manual check before activate tariff;
  * schema promotions, table compensations, columns:
    * compensation_id - compensation identifier;
    * name - compensation name;
  * schema promotions, table compensation_conditions, columns:
    * compensation_id - compensation identifier;
    * compensation_condition_id - compensation condition identifier;
    * tarif_id - internet tariff identifier;
    * tv_tarif_id - TV tariff identifier;
    * service_group_id - service group identifier;
    * service_id - service (type) identifier;
    * part - compensation part;
    * active_from - minimal activation date of tariff or service;
    * svc_cost - service cost;
    * force - allow force compensations.
* How to run tests
```bash
source venv/bin/activate
python -m pytest tests/
# verbose:
python -m pytest -v tests/
```
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact