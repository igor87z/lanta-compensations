import logging
import traceback
from argparse import ArgumentParser, Namespace
from datetime import date
from itertools import repeat
from multiprocessing import Process
from typing import Optional, Tuple

import psycopg2
import psycopg2.extras
from psycopg2.extensions import cursor

from compensations import (CashBox, Client, ClientFactory,
                           Compensation, CompensationFactory)
from compensations.logs.decorators import log
from compensations.logs.logger import logger

__VERSION__ = '2.0'


def arguments() -> Namespace:
    """Parse script's arguments"""
    if not hasattr(arguments, '__args'):
        parser = ArgumentParser(
            description='Make compensations for equipment rent.')

        parser.add_argument(
            '--db-host', required=True, type=str,
            help=(
                'database host name or ip address, required. '
                'Example: --db-host 127.0.0.1'))
        parser.add_argument(
            '--db-name', required=True, type=str,
            help=(
                'database name, required. Example: --db-name database'))
        parser.add_argument(
            '--db-user', required=True, type=str,
            help=(
                'login for connect to database, required. '
                'Example: --db-user username'))
        parser.add_argument(
            '--db-pass', required=True, type=str,
            help=(
                'password for connect to database, required. '
                'Example: --db-pass password'))
        parser.add_argument(
            '--db-port', default=5432, type=int,
            help=(
                'port for connect to database, optional. Default: 5432. '
                'Example: --db-port 5432'))
        parser.add_argument(
            '--procs', default=1, type=int,
            help=(
                'processes count, optional. Default: 1. Example: --procs 5'))
        parser.add_argument(
            '--parts', default=(), type=int, nargs='+',
            help=(
                'processing parts separated by space, optional. '
                'Start from zero(0). All values equal or greater and '
                'negative processes\' number ignoring. '
                'By default processing all parts. '
                'Example: --parts 0 3 4'))
        parser.add_argument(
            '--part-size', default=1000, type=int,
            help=(
                'clients get from database by group by set size. '
                'Default: 1000. Example, --part-size 1000'))
        parser.add_argument(
            '--compensations', default=(), type=int, nargs='+',
            help=(
                'processing compensations identifiers separated '
                'by space, optional. By default processing '
                'all compensations. Example: --compensations 1 2'))
        parser.add_argument(
            '--conditions', default=(), type=int, nargs='+',
            help=(
                'processing conditions identifiers separated '
                'by space, optional. By default processing '
                'all compensations. Example: --conditions 1, 2'))
        parser.add_argument(
            '--clients', default=(), type=int, nargs='+',
            help=(
                'clients\' identifiers. Processing only set clients. '
                'If set, for --procs, --parts using default values, '
                '--part-size equal set clients.'))
        parser.add_argument(
            '--disable-invoice', action='store_true',
            help=(
                'if set disable invoice bills, just count. '
                'Default: False, invoicing bills. '
                'Example, --disable-invoice'))
        parser.add_argument(
            '--log-level', default=20, type=int,
            choices=[0, 10, 20, 30, 40, 50],
            help=(
                'log level: 0 - NOTSET, 10 - DEBUG, 20 - INFO, '
                '30 - WARNING, 40 - ERROR, 50 - CRITICAL. '
                'Default: 20. Example, --log-level 10'))
        parser.add_argument(
            '--log-file', type=str,
            help=(
                'log file\'s path. By default log print to stdout/stderr.'))
        arguments.__args = parser.parse_args()
    return arguments.__args


@log('Processing client', level=logging.DEBUG)
def processing_client(db_cursor: cursor, client: Client,
                      compensations: Tuple[Compensation, ...],
                      disable_invoice: bool) -> None:
    """Make compensation bills for client.

    Keyword arguments:
    db_cursor -- database connection's cursor
    client -- processing client
    compensations -- exists compensations
    disable_invoice -- disable make invoice query, just count
    """
    bills = client.compensations(compensations)
    if bills:
        try:
            CashBox().invoice(db_cursor, bills, disable_invoice)
        except Exception as ex:
            logger().exception(ex)
            logger().exception(traceback.format_exc())


@log('Compensate services', level=logging.DEBUG)
def compensate(db_host: str, db_name: str, db_port: int, db_user: str,
               db_pass: str, part_number: int, part_size: int,
               parts: int, client_ids: Optional[Tuple[int, ...]],
               compensations: Optional[Tuple[int, ...]],
               conditions: Optional[Tuple[int, ...]],
               disable_invoice: bool) -> None:
    """Compensate services.

    Keyword arguments:
    db_host -- database host
    db_name -- database name
    db_port -- database host's port
    db_user -- database username
    db_pass -- database password
    part_number -- processing part's number
    part_size -- processing part size
    parts -- total parts
    client_ids -- processing clients' identifiers
    compensations -- compensations identifiers
    conditions -- conditions identifiers
    disable_invoice -- disable make invoice query, just count
    """
    conn = psycopg2.connect(host=db_host, dbname=db_name, port=db_port,
                            user=db_user, password=db_pass)
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    try:
        compensations = CompensationFactory().compensations(
            cur, compensations, conditions)
        for clients in ClientFactory().clients(cur, part_number, parts,
                                               part_size, client_ids):
            tuple(map(
                processing_client,
                repeat(cur),
                clients,
                repeat(compensations),
                repeat(disable_invoice)
            ))
    except Exception as ex:
        logger().exception(ex)
        logger().exception(traceback.format_exc())
    finally:
        cur.close()
        conn.close()


@log('Run process', level=logging.DEBUG)
def run_process(process_number: int) -> Process:
    """Start process.

    Keyword arguments:
    process_number -- current process' number
    """
    proc = Process(
        target=compensate,
        args=(
            arguments().db_host,
            arguments().db_name,
            arguments().db_port,
            arguments().db_user,
            arguments().db_pass,
            arguments().procs,
            arguments().part_size,
            process_number,
            tuple(),
            arguments().compensations,
            arguments().conditions,
            arguments().disable_invoice,
        )
    )
    proc.start()
    return proc


@log('Generate runnable parts tuple', level=logging.DEBUG)
def runnable() -> Tuple[int, ...]:
    """Generate runnable parts tuple."""
    return tuple(
        sorted(
            filter(
                lambda x: 0 <= x < arguments().procs,
                arguments().parts or tuple(range(0, arguments().procs))
            )
        )
    )


@log('Starting processes', level=logging.DEBUG)
def start_processes() -> Optional[Tuple[Process, ...]]:
    """Starting processes"""
    if arguments().clients or arguments().procs == 1:
        compensate(
            arguments().db_host,
            arguments().db_name,
            arguments().db_port,
            arguments().db_user,
            arguments().db_pass,
            1,
            (
                len(arguments().clients) if arguments().clients
                else arguments().part_size
            ),
            0,
            tuple(arguments().clients),
            arguments().compensations,
            arguments().conditions,
            arguments().disable_invoice,
        )
        return tuple()
    return tuple(
        map(
            run_process,
            runnable()
        )
    )


if __name__ == '__main__':
    logger(arguments().log_level, arguments().log_file).debug(
        'Run Compensation v.%s Arguments: %s', __VERSION__, arguments()
    )
    map(
        lambda proc: proc.join(),
        start_processes()
    )
