from datetime import datetime

import pytest

from compensations import Bill, Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def router_compensation_tv_tariff_26():
    return Compensation(
        compensation_id=1,
        name='Компенсация аренды роутера',
        conditions=(
            Condition(
                condition_id=2,
                tariff_id=None,
                tv_tariff_id=26,
                service_group_id=None,
                service_id=123,
                cost=50,
                activate=None,
                part=1
            ),
        )
    )


@pytest.fixture
def client_tariff_without_compensation():
    return Client(
        client_id=1,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=1,
                client_service_id=1,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(
            Bill(
                client_id=1,
                bill_type_id=1,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Компенсация аренды роутера',
                amount=-50.0
            ),
        ),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_with_compensation():
    return Client(
        client_id=1,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=1,
                client_service_id=1,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(
            Bill(
                client_id=1,
                bill_type_id=1,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Компенсация аренды роутера',
                amount=-50.0),
        ),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_without_compensation():
    return Client(
        client_id=2,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=2,
                client_service_id=2,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_blocked():
    return Client(
        client_id=2,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=True,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=2,
                client_service_id=2,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_auto_blocked():
    return Client(
        client_id=2,
        balance=410.55,
        credit=0,
        debt=10.0,
        bonus=0.0,
        blocked=True,
        auto_blocked=True,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=2,
                client_service_id=2,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_new_contract():
    return Client(
        client_id=2,
        balance=0,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=True,
        auto_blocked=False,
        new_contract=True,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2021, 3, 1, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=2,
                client_service_id=2,
                service=Service(
                    service_id=123,
                    group_id=None,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2021, 3, 1, 12, 00, 00),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=2,
                bill_type_id=4,
                ext_id=123,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def compensation_bill():
    return Bill(
        client_id=2,
        bill_type_id=7,
        ext_id=123,
        ext_id_old=1,
        ext_id_old2=2,
        comment='Компенсация аренды роутера',
        amount=-50
    )


def test_exists_tariff(router_compensation_tv_tariff_26,
                       client_tariff_without_compensation):
    assert client_tariff_without_compensation.compensations(
        (router_compensation_tv_tariff_26, )) == (), (
            'Лишняя компенсация, выставлен интернет тариф без таковой')


def test_exists_compensation(router_compensation_tv_tariff_26,
                             client_with_compensation):
    assert client_with_compensation.compensations(
        (router_compensation_tv_tariff_26, )) == (), 'Лишняя компенсация, уже есть'


def test_need_compensation(router_compensation_tv_tariff_26,
                           client_without_compensation, compensation_bill):
    assert client_without_compensation.compensations(
        (router_compensation_tv_tariff_26, )) == (
            compensation_bill,), 'Компенсация не выставлена'


def test_blocked(router_compensation_tv_tariff_26,
                 client_blocked):
    assert client_blocked.compensations(
        (router_compensation_tv_tariff_26, )) == (), (
            'Лишняя компенсация, аккаунт заблокирован')


def test_auto_blocked(router_compensation_tv_tariff_26,
                      client_auto_blocked, compensation_bill):
    assert client_auto_blocked.compensations(
        (router_compensation_tv_tariff_26, )) == (
            compensation_bill, ), ('Компенсация не выставлена заблокированном '
                                   'по долгу за рассрочку')


def test_new_contract(router_compensation_tv_tariff_26,
                      client_new_contract, compensation_bill):
    assert client_new_contract.compensations(
        (router_compensation_tv_tariff_26, )) == (
            compensation_bill, ), ('Компенсация не выставлена по новому '
                                   'не активированному договору')
