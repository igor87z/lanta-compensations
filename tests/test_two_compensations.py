from datetime import datetime

import pytest

from compensations import Bill, Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def router_compensation_tariff_366():
    return Compensation(
        compensation_id=1,
        name='Компенсация аренды роутера',
        conditions=(
            Condition(
                condition_id=1,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=50,
                activate=datetime(2019, 1, 15, 0, 0),
                part=1
            ),
            Condition(
                condition_id=67,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=datetime(2019, 1, 15, 0, 0),
                cost=200,
                activate=None,
                part=1
            ),
            Condition(
                condition_id=100,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=None,
                service_id=269,
                cost=200,
                activate=None,
                part=1
            ),
        )
    )


@pytest.fixture
def stb_compensation_force():
    return Compensation(
        compensation_id=2,
        name='Компенсация аренды приставки',
        conditions=(
            Condition(
                condition_id=300,
                tariff_id=None,
                tv_tariff_id=None,
                service_group_id=5,
                service_id=None,
                cost=None,
                activate=None,
                part=1,
                force=True
            ),
        )
    )


@pytest.fixture
def onu_compensation_tariff_366():
    return Compensation(
        compensation_id=3,
        name='Компенсация аренды ONU',
        conditions=(
            Condition(
                condition_id=115,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=12,
                service_id=None,
                cost=None,
                activate=datetime(2020, 12, 16, 0, 0),
                part=1
            ),
        )
    )


@pytest.fixture
def client_with_compensations():
    return Client(
        client_id=3126,
        balance=-19.02,
        credit=40,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=366,
            tariff_change=datetime(2020, 4, 1, 0, 0),
            tv_tariff_id=None,
            tv_tariff_change=datetime(2020, 4, 1, 0, 0),
        ),
        services=(
            ClientService(
                client_id=3126,
                client_service_id=75996,
                service=Service(
                    service_id=295,
                    group_id=5,
                    svc_name='Аренда Медиацентр TVIP S-Box 605, полцены',
                    svc_cost=75.0
                ),
                cs_cost=None,
                activate=datetime(2021, 5, 24, 16, 2, 29, 353712),
                compensation=True
            ),
            ClientService(
                client_id=3126,
                client_service_id=75997,
                service=Service(
                    service_id=295,
                    group_id=5,
                    svc_name='Аренда Медиацентр TVIP S-Box 605, полцены',
                    svc_cost=75.0
                ),
                cs_cost=None,
                activate=datetime(2021, 5, 24, 16, 2, 43, 542803),
                compensation=True
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=3126,
                bill_type_id=4,
                ext_id=295,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда Медиацентр TVIP S-Box 605, полцены',
                amount=19.35
            ),
            Bill(
                client_id=3126,
                bill_type_id=4,
                ext_id=295,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда Медиацентр TVIP S-Box 605, полцены',
                amount=19.35
            ),
        )
    )


@pytest.fixture
def compensate_bills():
    return (
        Bill(
            client_id=3126,
            bill_type_id=1,
            ext_id=295,
            ext_id_old=2,
            ext_id_old2=75996,
            comment='Компенсация аренды приставки',
            force=True,
            amount=-19.35
        ),
        Bill(
            client_id=3126,
            bill_type_id=1,
            ext_id=295,
            ext_id_old=2,
            ext_id_old2=75997,
            comment='Компенсация аренды приставки',
            force=True,
            amount=-19.35
        ),
    )


def test_two_need(router_compensation_tariff_366,
                  stb_compensation_force,
                  onu_compensation_tariff_366,
                  client_with_compensations,
                  compensate_bills):
    assert client_with_compensations.compensations(
        (router_compensation_tariff_366, stb_compensation_force,
         onu_compensation_tariff_366)) == compensate_bills, 'Нет необходимых компенсаций'
