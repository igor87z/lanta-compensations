from datetime import datetime

import pytest

from compensations import Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def router_compensation_tariff_366():
    return Compensation(
        compensation_id=1,
        name='Компенсация аренды роутера',
        conditions=(
            Condition(
                condition_id=1,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=50,
                activate=datetime(2019, 1, 15),
                part=1
            ),
            Condition(
                condition_id=2,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=200,
                activate=datetime(2019, 1, 15),
                part=1
            ),
        )
    )


@pytest.fixture
def client_activated_before_limit():
    return Client(
        client_id=75350,
        balance=0,
        credit=0,
        debt=0.0,
        bonus=540.59,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=366,
            tariff_change=datetime(2018, 11, 1, 0, 0),
            tv_tariff_id=None,
            tv_tariff_change=None
        ),
        services=(
            ClientService(
                client_id=1,
                client_service_id=1,
                service=Service(
                    service_id=123,
                    group_id=6,
                    svc_name='Аренда роутера',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 3, 2, 19, 48, 19, 845399),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=()
    )


def test_activate_before_limit(router_compensation_tariff_366,
                               client_activated_before_limit):
    assert client_activated_before_limit.compensations(
        (router_compensation_tariff_366, )) == (), (
            'Лишняя компенсация, и услуга, и тариф '
            'выставлен раньше минимального срока')
