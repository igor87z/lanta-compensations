from datetime import datetime

import pytest

from compensations import Bill, Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def stb_compensation():
    return Compensation(
        compensation_id=2,
        name='Компенсация аренды приставки',
        conditions=(
            Condition(
                condition_id=136,
                tariff_id=None,
                tv_tariff_id=None,
                service_group_id=5,
                service_id=None,
                part=1,
                activate=None,
                cost=None,
                force=True
            ),
        )
    )


@pytest.fixture
def onu_compensation_tariff_366():
    return Compensation(
        compensation_id=3,
        name='Компенсация аренды ONU',
        conditions=(
            Condition(
                condition_id=115,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=12,
                service_id=None,
                cost=None,
                activate=datetime(2020, 12, 16, 0, 0),
                part=1
            ),
        )
    )


@pytest.fixture
def client_without_compensations():
    return Client(
        client_id=83250,
        balance=44.79,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=None,
            tariff_change=datetime(2019, 7, 1, 0, 0),
            tv_tariff_id=None,
            tv_tariff_change=datetime(2019, 6, 1, 0, 0),
        ),
        services=(
            ClientService(
                client_id=83250,
                client_service_id=76033,
                service=Service(
                    service_id=207,
                    group_id=5,
                    svc_name='Аренда Медиацентр TVIP S-Box 605',
                    svc_cost=150.0
                ),
                cs_cost=None,
                activate=datetime(2021, 5, 25, 12, 8, 56, 901022),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=()
    )


@pytest.fixture
def compensate_bills():
    return ()


def test_two_need(stb_compensation,
                  client_without_compensations,
                  compensate_bills):
    assert client_without_compensations.compensations(
        (stb_compensation, )) == compensate_bills, 'Лишняя компенсация'
