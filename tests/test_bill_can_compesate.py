from datetime import datetime

import pytest

from compensations import Bill, NONE_BILL


@pytest.fixture
def service_bill():
    return Bill(
        client_id=45535,
        bill_type_id=4,
        ext_id=101,
        ext_id_old=None,
        ext_id_old2=None,
        comment='Аренда роутера SNR-CPE-W4N',
        amount=50.0
    )


@pytest.fixture
def service_half_bill():
    return Bill(
        client_id=45535,
        bill_type_id=4,
        ext_id=101,
        ext_id_old=None,
        ext_id_old2=None,
        comment='Аренда роутера SNR-CPE-W4N',
        amount=25.0
    )


@pytest.fixture
def compensate_bill():
    return Bill(
        client_id=45535,
        bill_type_id=1,
        ext_id=101,
        ext_id_old=1,
        ext_id_old2=33467,
        comment='Компенсация аренды роутера',
        amount=-50.0
    )


@pytest.fixture
def not_compensate_bill():
    return Bill(
        client_id=45535,
        bill_type_id=1,
        ext_id=158,
        ext_id_old=1,
        ext_id_old2=33467,
        comment='Компенсация аренды роутера',
        amount=-50.0
    )


def test_compensate_bill(service_bill, compensate_bill):
    assert service_bill.can_compensate(
        compensate_bill), 'Счет должен быть компенсирован'


def test_not_compensate_bill(service_bill, not_compensate_bill):
    assert not service_bill.can_compensate(
        not_compensate_bill), 'Счет не должен быть компенсирован, разные услуги'


def test_half_bill(service_half_bill, compensate_bill):
    assert not service_half_bill.can_compensate(
        compensate_bill), 'Счет не должен быть компенсирован, разная сумма'


def test_half_bill(compensate_bill):
    assert not NONE_BILL.can_compensate(
        compensate_bill), 'Счет не должен быть компенсирован, нуловый объект'
