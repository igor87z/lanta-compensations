from datetime import datetime

import pytest

from compensations import Bill, Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def router_compensation_tariff_365():
    return Compensation(
        compensation_id=1,
        name='Компенсация аренды роутера',
        conditions=(
            Condition(
                condition_id=2,
                tariff_id=365,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=50,
                activate=None,
                part=1
            ),
            Condition(
                condition_id=2,
                tariff_id=365,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=200,
                activate=None,
                part=1
            ),
        )
    )


@pytest.fixture
def client_with_compensation():
    return Client(
        client_id=45535,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=365,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=45535,
                client_service_id=33467,
                service=Service(
                    service_id=101,
                    group_id=6,
                    svc_name='Аренда роутера SNR-CPE-W4N',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(
            Bill(
                client_id=45535,
                bill_type_id=1,
                ext_id=101,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Компенсация аренды роутера',
                amount=-50.0
            ),
        ),
        service_bills=(
            Bill(
                client_id=45535,
                bill_type_id=4,
                ext_id=101,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def client_without_compensation():
    return Client(
        client_id=45535,
        balance=410.55,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=False,
        auto_blocked=False,
        new_contract=False,
        tariff=Tariff(
            tariff_id=365,
            tariff_change=datetime(2018, 12, 5, 0, 0),
            tv_tariff_id=26,
            tv_tariff_change=datetime(2018, 12, 5, 0, 0),
        ),
        services=(
            ClientService(
                client_id=45535,
                client_service_id=33467,
                service=Service(
                    service_id=101,
                    group_id=6,
                    svc_name='Аренда роутера SNR-CPE-W4N',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2018, 12, 8, 19, 51, 43, 40704),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=45535,
                bill_type_id=4,
                ext_id=101,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def compensate_bill():
    return Bill(
        client_id=45535,
        bill_type_id=1,
        ext_id=101,
        ext_id_old=1,
        ext_id_old2=33467,
        comment='Компенсация аренды роутера',
        amount=-50.0
    )


def test_need(router_compensation_tariff_365, client_with_compensation):
    assert client_with_compensation.compensations(
        (router_compensation_tariff_365,)) == (), 'Лишняя компенсация, уже есть'


def test_not_need(router_compensation_tariff_365,
                  client_without_compensation, compensate_bill):
    assert client_without_compensation.compensations(
        (router_compensation_tariff_365,)) == (
            compensate_bill, ), 'Нет необходимой компенсации'
