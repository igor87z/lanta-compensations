from datetime import datetime

import pytest

from compensations import Bill, Client, ClientService, Compensation, \
    Condition, Service, Tariff


@pytest.fixture
def router_compensation_tariff_366():
    return Compensation(
        compensation_id=1,
        name='Компенсация аренды роутера',
        conditions=(
            Condition(
                condition_id=1,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=50,
                activate=datetime(2019, 1, 15),
                part=1
            ),
            Condition(
                condition_id=2,
                tariff_id=366,
                tv_tariff_id=None,
                service_group_id=6,
                service_id=None,
                cost=200,
                activate=datetime(2019, 1, 15),
                part=1
            ),
        )
    )


@pytest.fixture
def debtor():
    return Client(
        client_id=65216,
        balance=-2450.83,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=True,
        auto_blocked=True,
        new_contract=False,
        tariff=Tariff(
            tariff_id=366,
            tariff_change=datetime(2019, 1, 26, 0, 0),
            tv_tariff_id=None,
            tv_tariff_change=None
        ),
        services=(
            ClientService(
                client_id=65216,
                client_service_id=19136,
                service=Service(
                    service_id=101,
                    group_id=6,
                    svc_name='Аренда роутера SNR-CPE-W4N',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2016, 8, 1, 10, 21, 56, 613000),
                compensation=False
            ),
            ClientService(
                client_id=65216,
                client_service_id=19137,
                service=Service(
                    service_id=108,
                    group_id=5,
                    svc_name='Аренда приставки с 06.10.2015',
                    svc_cost=150.0
                ),
                cs_cost=None,
                activate=datetime(2016, 8, 1, 10, 22, 9, 647000),
                compensation=False
            ),
        ),
        exists_compensations=(),
        service_bills=()
    )


@pytest.fixture
def restored_debtor():
    return Client(
        client_id=65216,
        balance=-2450.83,
        credit=0,
        debt=0.0,
        bonus=0.0,
        blocked=True,
        auto_blocked=True,
        new_contract=True,
        tariff=Tariff(
            tariff_id=366,
            tariff_change=datetime(2019, 1, 26, 0, 0),
            tv_tariff_id=None,
            tv_tariff_change=None
        ),
        services=(
            ClientService(
                client_id=65216,
                client_service_id=19136,
                service=Service(
                    service_id=101,
                    group_id=6,
                    svc_name='Аренда роутера SNR-CPE-W4N',
                    svc_cost=50.0
                ),
                cs_cost=None,
                activate=datetime(2016, 8, 1, 10, 21, 56, 613000),
                compensation=False
            ),
            ClientService(
                client_id=65216,
                client_service_id=19137,
                service=Service(
                    service_id=108,
                    group_id=5,
                    svc_name='Аренда приставки с 06.10.2015',
                    svc_cost=150.0
                ),
                cs_cost=None,
                activate=datetime(2016, 8, 1, 10, 22, 9, 647000),
                compensation=False
            )
        ),
        exists_compensations=(),
        service_bills=(
            Bill(
                client_id=65216,
                bill_type_id=4,
                ext_id=101,
                ext_id_old=None,
                ext_id_old2=None,
                comment='Аренда роутера SNR-CPE-W4N',
                amount=50.0
            ),
        )
    )


@pytest.fixture
def compensate_bill():
    return Bill(
        client_id=65216,
        bill_type_id=1,
        ext_id=101,
        ext_id_old=1,
        ext_id_old2=19136,
        comment='Компенсация аренды роутера',
        amount=-50.0
    )


def test_debtor(router_compensation_tariff_366, debtor):
    assert debtor.compensations((router_compensation_tariff_366,)) == (), (
        'Лишняя компенсация, клиент должник')


def test_restore_debtor(router_compensation_tariff_366, restored_debtor,
                        compensate_bill):
    assert restored_debtor.compensations(
        (router_compensation_tariff_366,)) == (
             compensate_bill,), (
                 'Нет компенсации, восстановленный договор, '
                 'работаем как с новым')
